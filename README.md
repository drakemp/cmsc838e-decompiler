# cmsc838e-decompiler
---

## Project structure

Root directory contains scripts, docker resources, and documentation. `src/` will contain
`Makefile`and plugin directories. All plugins will reside in their own respective folders named
after the plugin itself. `jmp` is a example plugin from the BAP readme. 

```
/
├── build.sh
├── Dockerfile
├── README.md
├── run.sh
└── src
    ├── jmp
    │   ├── _build
    │   │   ├── _digests
    │   │   ...
    │   │   ├── jmp.plugin
    │   │   ├── _log
    │   │   └── ocamlc.where
    │   ├── jmp.ml
    │   └── jmp.plugin -> /src/jmp/_build/jmp.plugin
    └── Makefile
```
