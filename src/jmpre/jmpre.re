

open Core_kernel;

open Bap_main;

open Bap.Std;

let counter = {
  as _;
  inherit class Term.visitor((int, int));
  pub! enter_term = (_, _, (jmps, total)) => (jmps, total + 1);
  pub! enter_jmp = (_, (jmps, total)) => (jmps + 1, total)
};

let main = (proj) => {
  let (jmps, total) = counter#run(Project.program(proj), (0, 0));
  printf("ratio = %d/%d = %g\n", jmps, total, float(jmps) /. float(total));
};

let () =
  Extension.declare @@
  (
    (_ctxt) => {
      Project.register_pass'(main);
      Ok();
    }
  );
