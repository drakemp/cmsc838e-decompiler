open Core_kernel
open Bap_main
open Bap.Std

open Format

open Common

let def_map_visitor = object
  inherit [DefExpMap.t] Term.visitor

  method! enter_def t map =
    map |>
    VarMap.add_exn
      ~key:(Def.lhs t)
      ~data:([t |> Def.rhs] |> ExpSet.of_list)
end

let propogate_mapper def_exp_map= object
  inherit Term.mapper as super

  val mutable def_exp_map = ref def_exp_map

  method! map_def def =
    def
    |> Def.free_vars
    |> Var.Set.fold
      ~init: def
      ~f:(fun def var ->
          VarMap.find !def_exp_map var
          |> Option.value_map
            ~default:def
            ~f:(fun exps ->
                if (exps |> ExpSet.length = 1)
                then
                  let def = Def.substitute
                      def
                      (Var var)
                      (ExpSet.nth exps 0 |> Option.value_exn) in
                  def_exp_map := VarMap.update
                      !def_exp_map
                      (def |> Def.lhs)
                      ~f:(fun _ -> [def |>Def.rhs] |> ExpSet.of_list);
                  def
                else def
              ))
end



let computed_def_map sub =
  def_map_visitor#visit_sub sub VarMap.empty

let main proj =
  let open Option.Let_syntax in
  let prog = Project.program proj in
  Program.lookup sub_t prog (Tid.for_name "entry")
  |> Option.value_map
    ~default:prog
    ~f:(fun entry ->
        Term.update
        sub_t
        prog
        (entry
        |> (propogate_mapper (entry |> computed_def_map))#map_sub))
  |> Project.with_program proj


let () = Extension.declare @@ fun _ctxt -> begin (
    Project.register_pass ~deps:["ssa"; "stack-var"] main;
    Ok ()) end
