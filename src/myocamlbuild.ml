open Ocamlbuild_plugin
open Command

let () =
  dispatch begin function
    | After_rules ->
    rule "Reasonml to Ocaml conversion"
          ~prod:"%.ml"
          ~dep:"%.re"
          begin fun env _build ->
            let arg = env "%.re"
            and out = env "%.ml" in
            Cmd(S[A"refmt"; P arg; A"-p"; A"ml"; Sh">"; P out])
          end
    | _ -> ()
  end
