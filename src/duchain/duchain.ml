open Core_kernel
open Bap_main
open Bap.Std

open Format

(* Taken from BAP's optimiaztion.ml *)
let def_use_collector = object
  inherit [Var.Set.t * Var.Set.t] Term.visitor

  method! enter_def t (defs,uses) =
    Set.add defs (Def.lhs t), Set.union uses (Def.free_vars t)

  method! enter_phi t (defs,uses) =
    Set.add defs (Phi.lhs t), Set.union uses (Phi.free_vars t)

  method! enter_jmp t (defs,uses) =
    defs, Set.union uses (Jmp.free_vars t)
end

let computed_def_use sub =
  def_use_collector#visit_sub sub (Var.Set.empty,Var.Set.empty)

let main proj =
  let  prog = Project.program proj in
  let (defs, use) =
    Program.lookup sub_t prog (Tid.for_name "entry")
    |> Option.value_map
      ~default: (Var.Set.empty,Var.Set.empty)
      ~f:computed_def_use
  in
  printf "%a\n" Var.pp_seq (defs |> Var.Set.to_sequence);
  printf "%a\n" Var.pp_seq (use |> Var.Set.to_sequence);
  ()


let () = Extension.declare @@ fun _ctxt -> begin (
    Project.register_pass' main;
    Ok ()) end
