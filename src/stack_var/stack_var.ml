open Core_kernel
open Bap_main
open Bap.Std

open Format

module ExpMap = Map.Make_binable(Exp)


let is_rsp reg = (reg |> Var.name) = "RSP"

let rec is_stack reg_exp =
  let open Bil.Types in
  match reg_exp with
  | Var(reg) -> reg |> is_rsp
  | BinOp(_, left, Int(_offset)) -> is_stack left
  | _ -> false

let is_mem reg = match (reg |> Var.typ) with | Mem(_, _) -> true | _ -> false
let is_zf reg = (String.sub (reg |> Var.name) ~pos:0 ~len:2) = "ZF"

let new_local_var mem offset =
  Var.create
    (sprintf "local_%Ld" (offset |> Int64.neg))
    (mem |> Var.typ)

let new_heap_var mem num (offset:int64) =
  Var.create
    (sprintf "heap_%d[%Ld]" num offset)
    (mem |> Var.typ)

let new_var mem (addr: Exp.t) var =
  begin match var with
    | Some(var) ->
      Var.with_index
        (Var.create
           (var |> Var.name)
           (mem |> Var.typ))
        ((var |> Var.index) + 1)
      |> Some
    | None -> begin
        match (addr: Exp.t) with
        | Var(reg) when reg |> is_rsp ->
          new_local_var mem Int64.zero
          |> Some
        | Var(reg) ->
          new_heap_var mem (reg |> Var.index) Int64.zero
          |> Some
        | BinOp(_, Var(reg), Int(offset)) when
            reg |> is_rsp ->
          new_local_var mem (offset |> Bitvector.to_int64_exn) |> Some
        | BinOp(_, Var(reg), Int(offset))->
          new_heap_var mem (reg |> Var.index) (offset |> Bitvector.to_int64_exn) |> Some
        | _ -> None
      end
  end




let stack_var_mapper = object (self)
  inherit Term.mapper as super

  val mutable map = ref (ExpMap.empty)

  method new_var mem addr =
    map := ExpMap.change
        !map
        addr
        ~f:(new_var mem addr);
    ExpMap.find_exn !map addr

  method! map_def def =
    let def = match def |> Def.lhs with
      | reg when reg |> is_mem -> begin
          match def |> Def.rhs with
          | Store(Var(mem), addr, exp, _, _) ->
            let def = Def.create (self#new_var mem addr) exp in
            def
          | _ -> def
        end
      | _ -> def in
    Def.with_rhs def (def |> Def.rhs |> super#map_exp)

  method! map_load ~mem ~addr endianness size =
    match ExpMap.find !map addr with
    | Some (var) -> Var(var)
    | None -> Load(mem, addr, endianness,size)

end

let main proj =
  let open Option.Let_syntax in
  let prog = Project.program proj in
  Program.lookup sub_t prog (Tid.for_name "entry")
  |> Option.value_map
    ~default:prog
    ~f:(fun entry ->
        Term.update
          sub_t
          prog
          (entry
           |> stack_var_mapper#map_sub))
  |> Project.with_program proj

let () = Extension.declare @@ fun _ctxt -> begin (
    Project.register_pass ~deps:["ssa"] main;
    Ok ()) end
