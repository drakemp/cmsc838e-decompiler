open Core_kernel
open Bap_main
open Bap.Std

open Format

module ExpExt = Comparable.Map_and_set_binable(Exp)

module ExpSet = struct
  include ExpExt.Set

  let pp ppf set = fprintf ppf "{%a}" Exp.pp_seq (set |> Set.to_sequence)
end

module VarMap = Map.Make_binable(Var)

module DefExpMap = struct
  type t = ExpSet.t VarMap.t[@@deriving bin_io, compare, sexp]

  let pp fmt map = map |> VarMap.to_sequence |> Sequence.iter ~f:(fun (var, exps) -> fprintf fmt "(%a: %a), " Var.pp var ExpSet.pp exps)
end
