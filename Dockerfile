FROM binaryanalysisplatform/bap:latest

RUN sudo apt update \
    && sudo apt install -y emacs vim xdot libzmq3-dev python3 python3-pip
RUN pip3 install jupyterlab jupyter
RUN opam install -y user-setup tuareg merlin reason jupyter \
    && opam user-setup install

ENV PATH="~/.local/bin:${PATH}"
RUN eval $(opam env) \
    ocaml-jupyter-opam-genspec \
    && ~/.local/bin/jupyter kernelspec install --user --name ocaml-jupyter "$(opam var share)/jupyter"

CMD /bin/bash
